from django.urls import path

from books import views

app_name = 'books'

urlpatterns = [
    path('', views.book_list, name='book_list'),
    path('create/', views.book_create, name='book_create'),
    path('update/<pk>/', views.book_update, name='book_update'),
    path('delete/<pk>', views.book_delete, name='book_delete'),
]